package com.example.grhm

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main2.*
import java.util.ArrayList

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        lineGraph2.showPopup = true
        lineGraph2.setBottomTextList(
            arrayListOf("2015", "2016", "2017", "2018", "2019", "2020", "2021")
        )
        lineGraph2.setDataList(
            arrayListOf(
                arrayListOf(94688, 96808, 88883, 82948, 73731, 79107, 73521),
                arrayListOf(94333, 95569, 91722, 92492, 91506, 111803, 125671)
            )
        )
        lineGraph2.setColorArray(intArrayOf(
            Color.parseColor("#FFAA55"),
            Color.parseColor("#55AAFF")
        ))

        barGraph2.setBottomTextList(arrayListOf("2006", "2007", "2008", "2008", "2010", "2011", "2012"))
        val s = arrayListOf(146064, 145861, 146048, 146349, 155196, 155477, 156136)
        val b = mutableListOf<Int>()
        for (i in s) {
            b.add((i-140000)*5)
        }
        barGraph2.setDataList(b as ArrayList<Int>?, (160000-140000)*5)
    }

    fun onClicked(v: View) {
        val intent = Intent(this, MainActivity3::class.java)
        startActivity(intent)
    }
}