package com.example.grhm

import android.content.Intent
import android.graphics.Color
import android.graphics.Interpolator
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.jjoe64.graphview.series.DataPoint
import kotlinx.android.synthetic.main.activity_main3.*

class MainActivity3 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)

        val s = arrayListOf(
            Entry(3f, -6f),
            Entry(7f, -10f),
            Entry(11f, -6f),
            Entry(14f, -3f),
            Entry(18f, -8f),
            Entry(22f, -10f),
            Entry(26f, -10f),
            Entry(28f, -8f),
            Entry(31f, -6f)
        )
        val c = LineDataSet(s, "Температура в Электростале")


        // draw dashed line
        //c.enableDashedLine(10f, 5f, 0f)

        // line thickness and point size
        c.lineWidth = 1f
        c.circleRadius = 10f

        // text size of values
        c.valueTextSize = 12f

        // draw points as solid circles
        c.setDrawCircleHole(false)
        val m = arrayListOf<ILineDataSet>()
        m.add(c)
        val data = LineData(m)
        lineGraph3.data = data


        val e = arrayListOf(PieEntry(71093f, "Мужчины"), PieEntry(85043f, "Женщины"))
        val p = PieDataSet(e, "Кол-во мужчин и женщин в 2012г в Электростале")

        p.sliceSpace = 10f
        p.valueTextSize = 20f
        p.setValueTextColors(listOf(Color.WHITE, Color.WHITE))
        p.colors = listOf(Color.RED, Color.BLUE)

        //dataSet.setSelectionShift(0f);
        val data2 = PieData(p)
        pieGraph.data = data2
    }

    fun onClicked(v: View) {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}