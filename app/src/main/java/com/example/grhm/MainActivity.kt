package com.example.grhm

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.jjoe64.graphview.GraphView
import com.jjoe64.graphview.LabelFormatter
import com.jjoe64.graphview.ValueDependentColor
import com.jjoe64.graphview.Viewport
import com.jjoe64.graphview.series.BarGraphSeries
import com.jjoe64.graphview.series.DataPoint
import com.jjoe64.graphview.series.LineGraphSeries
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        lineGraph.title = "Статистика коронавируса в Московской области по дням"
        val s = LineGraphSeries(
            arrayOf(
                DataPoint(0.0, 1180.0),
                DataPoint(4.0, 899.0),
                DataPoint(8.0, 855.0),
                DataPoint(11.0, 1990.0),
                DataPoint(15.0, 2626.0),
                DataPoint(19.0, 6400.0),
                DataPoint(23.0, 6709.0),
                DataPoint(25.0, 5769.0),
                DataPoint(28.0, 11470.0)
            )
        )
        lineGraph.addSeries(s)
        lineGraph.gridLabelRenderer.labelFormatter = object: LabelFormatter {
            override fun formatLabel(value: Double, isValueX: Boolean): String {
                if (!isValueX) { return value.toInt().toString() }
                return "${value.toInt()+3} янв."
            }

            override fun setViewport(viewport: Viewport?) {}
        }
        lineGraph.viewport.isScalable = true
        lineGraph.viewport.setScalableY(true)



        barGraph.title = "Динамика выздоровления в Московской области по дням"
        val m = BarGraphSeries(
            arrayOf(
                DataPoint(0.0, 1216.0),
                DataPoint(1.0, 1319.0),
                DataPoint(2.0, 1392.0),
                DataPoint(3.0, 1764.0),
                DataPoint(4.0, 1558.0),
                DataPoint(5.0, 3112.0),
                DataPoint(6.0, 4495.0)
            )
        )
        m.valueDependentColor = object: ValueDependentColor<DataPoint> {
            override fun get(data: DataPoint?): Int {
                if ((data!!.x).toInt()%2==0) { return Color.BLUE }
                return Color.MAGENTA
            }

        }
        barGraph.addSeries(m)
        barGraph.gridLabelRenderer.labelFormatter = object: LabelFormatter {
            override fun formatLabel(value: Double, isValueX: Boolean): String {
                if (!isValueX) { return value.toInt().toString() }
                return "${value.toInt()+25} янв."
            }

            override fun setViewport(viewport: Viewport?) { }
        }
        barGraph.viewport.isScalable = true
        barGraph.viewport.setScalableY(true)
    }

    fun onClicked(v: View) {
        val intent = Intent(this, MainActivity2::class.java)
        startActivity(intent)
    }
}